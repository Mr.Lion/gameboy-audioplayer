var i=0;

class AudioPlayer{
    
    constructor(src,controls){
        this.src=src[0];
        this.audio= new Audio(this.src);
        this.controls=controls;
        this.initControls(src);
    }
    initControls(src){
        this.initPlay(src,controls);
        
    }
    initPlay(src,controls){
        controls.Play.onclick=() =>{
        
            this.play();
        }
        controls.stop.onclick=() =>{
        
            this.pause();
        }
        controls.volumenUp.onclick= () => {
            if(this.audio.volume==1){
                this.audio.volume-=0.1;
            }
            this.audio.volume+=0.1;
        }
        controls.volumenDown.onclick= () =>{
            if(this.audio.volume==0){
                this.audio.volume+=0.1;
            }
            this.audio.volume-=0.1;
        }
        controls.next.onclick= () =>{
            this.audio.pause();
            
            if(!this.audio.pause()){
                if(i>2){
                    i=0;

                }
                this.cancion=src[i++];
                this.audio= new Audio(this.cancion);
                this.audio.play();
                /*this.audio.ontimeupdate= () => {this.updateUI();}*/
                console.log("derecha: ",i);

            }
        }
        controls.previous.onclick= () =>{
            this.audio.pause();

            if(!this.audio.pause()){
                if(i<0){
                    i=2;
                }
                this.cancion=src[i--];
                this.audio= new Audio(this.cancion);
                this.audio.play();
                /*this.audio.ontimeupdate= () => {this.updateUI();}*/
                console.log("izq: ",i);

            }
        }
        controls.avanzar.onclick=()=>{
            
            this.audio.currentTime+=10;

            
        }
        
        controls.retroceder.onclick=()=>{
            this.audio.currentTime-=10;
        }
        
    }
    
    play(){
        this.audio.play();
    }
    pause(){
        this.audio.pause();     
    }

}