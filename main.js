const btnPlay = document.querySelector("#player .controls .btnPlay");
const btnStop= document.querySelector("#player .controls .btnStop");
const volumenUp= document.querySelector("#player .controls .cruz2");
const volumenDown= document.querySelector("#player .controls .cruz4");
const avanzar= document.querySelector("#player .controls .cruz1");
const retroceder= document.querySelector("#player .controls .cruz3");
const previous= document.querySelector("#player .controls .boton3");
const next= document.querySelector("#player .controls .boton4");
const progress= document.querySelector("#player .screen .progress");
const songs=["../canciones/1.mp3","../canciones/2.mp3","../canciones/3.mp3"];

const controls={
    Play: btnPlay,
    stop: btnStop,
    volumenUp: volumenUp,
    volumenDown: volumenDown,
    avanzar:avanzar,
    retroceder:retroceder,
    previous:previous,
    next:next,
    progress:progress

}
const player=new AudioPlayer(songs,controls);
    